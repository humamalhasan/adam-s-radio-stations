Rails.application.routes.draw do
  scope ":locale", locale: /#{I18n.available_locales.join("|")}/ do 

    get 'terms', to: "pages#terms"

    get 'privacy', to: "pages#privacy"

    get 'about', to: "pages#about"

    devise_scope :podcast do
     get "signup", to: "devise/registrations#new"
     get "signin", to: "devise/sessions#new"
     get "signout", to: "devise/sessions#destroy"
    end

    devise_for :podcasts

    resources :podcasts, only: [:index, :show] do 
    	resources :episodes
    end

    authenticated :podcast do 
    	root 'podcasts#dashboard', as: "authenticated_root"
    end

    root 'welcome#index' 
  end

  get '*path', to: redirect("/#{I18n.default_locale}/%{path}")
  get '',      to: redirect("/#{I18n.default_locale}")
end
