class AddCategoryIdToEpisodes < ActiveRecord::Migration[5.1]
  def change
    add_column :episodes, :category_id, :integer
  end
end
